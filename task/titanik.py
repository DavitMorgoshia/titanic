import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    ''' 
    titles = ["Mr.", "Mrs.", "Miss."]
    results = []
    for title in titles:
        
        # Calculate the number of missed values    
        num_missed_values = sum(df.loc[df['Name'].str.contains(title)]['Age'].isnull())

        
        # Calculate the rounded median age
        median_age = df.loc[df['Name'].str.contains(title)]['Age'].median(skipna=True)
        
        
        # Append the results to the list
        results.append((title, num_missed_values, int(median_age)))

    return results
